from rest_framework.serializers import (
    CharField,
	ModelSerializer,
    SerializerMethodField,
    ValidationError,
)

from herohub_dynamic.serializers import (
    ValidateDataTypes,
)

from crowdhero_dbom.models import (
    Users,
)

from herohub_dynamic.models import (
    CustomFieldLookup,
    DataTypes,
    FieldTypes,
)

from django.db.models import Q

from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from rest_framework.renderers import JSONRenderer
from rest_framework import serializers

class ForgotpasswordSerializer(ModelSerializer):
	email_address = CharField(required=False, allow_blank=True)
    mobile_number = CharField(required=False, allow_blank=True))
	class Meta:
		model = Users
		fields = ['email_address', 'mobile_number']

	def validate(self, data):
		users_obj = None
		email_address = data.get("email_address", None)
		mobile_number = data.get("mobile_number", None)
		#message1 = Message.objects.get(message_id = '1')
		#message2 = Message.objects.get(message_id = '2')
		if email_address or mobile_number:
			raise ValidationError("Please fill out with Email Address/Mobile Number")
		users = Users.objects.filter(Q(email_address=email_address)).filter(Q(mobile_number=mobile_number)).distinct()
		users = users.exclude(email_address__isnull=True).exclude(email_address__iexact='').exclude(mobile_number__isnull=True).exclude(mobile_number__iexact='')
		if users.exists() and users.count() == 1:
			users_obj = users.first()
			
		else:
			raise ValidationError("Email Address/Mobile Number is not valid")

		return users_obj
