import datetime

from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)

from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    CreateAPIView,
    UpdateAPIView,
    DestroyAPIView,
)

from drf_multiple_model.views import (
    ObjectMultipleModelAPIView,
)

from crowdhero_dbom.models import (
    Users,
)

from herohub_dynamic.models import (
    CustomFieldLookup,
    DataTypes,
    FieldTypes,
)

from rest_framework.permissions import(
    AllowAny,
	IsAuthenticated,
	IsAdminUser,
	IsAuthenticatedOrReadOnly,
	)

from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from .serializer import (ForgotpasswordSerializer,)

class ForgotpasswordAPIView(APIView):
    queryset = Users.objects.all()
    permissions_classes = [AllowAny]
    serializer_class = ForgotpasswordSerializer
   
    def post(self, request):
        data = request.data 
        serializer = ForgotpasswordSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            return Response(new_data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)




